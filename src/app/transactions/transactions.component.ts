/*
Component to handle fetching of database information.
@Author Majd Asab + Rami Shehto
*/
import { Component, OnInit } from '@angular/core';
import { ServiceCall } from '../serviceCall';
import { Table } from './table';


@Component({
  selector: 'transactions-table',
  templateUrl: './transactions.component.html',
  styles: []
})
export class TransactionsComponent implements OnInit {

  jsonData: Table[];
  settings = {
    actions:false,
    columns: {
      trxID: {
        title: 'ID'
      },
      equityName: {
        title: 'Equity'
      },
      equityAmount: {
        title: 'Amount'
      },
      equityPrice: {
        title: "Price"
      },
      trxType: {
        title: "Transaction type"
      },
      strategy: {
        title: "Strategy"
      },
      strategyParameters: {
        title: "Strategy Paramters"
      },
      profit: {
        title: "Profit"
      },
      timeStamp: {
        title: "Timestamp"
      }

    }
  };


  constructor(private serviceCall: ServiceCall) { }

  ngOnInit() {
    this.refresh();
  }

  refresh(){
    this.serviceCall.getAllDbInfo().subscribe(
      (data: Table[]) => {
        this.jsonData = data;
         console.log("Refresh successful");
      },
      error => console.log('Database info fetching error', error)
    );
  }

}
