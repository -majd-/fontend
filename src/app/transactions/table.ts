/*
Interface used to map to the returned response from the database
@Author Majd Asab + Rami Shehto
*/
export interface Table{
  trxID:      number;
  equityName:         string;
  equityAmount:       number;
  equityPrice:        number;
  trxType:            string;
  strategy:           string;
  strategyParameters: string;
  profit:             number;
  timeStamp:          string;
}
