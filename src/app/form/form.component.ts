/*
Component for handling strategy submissions.
@Author Majd Asab + Rami Shehto
*/

import { Component} from '@angular/core';
import { ServiceCall } from "../serviceCall";
import { DummyModel } from "./dummyModel";

@Component({
  selector: 'two-moving-averages-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent {
  constructor(private serviceCall : ServiceCall){}
  url= 'http://localhost:8080';
  killActive: boolean = false;
  model = new DummyModel("C",1,3,2,4,5);
  isValid: boolean = false;
  equity="C";
  amount=1;
  short=2;
  long=3;
  profitCap=4;
  lossCap=5;

  onSubmit() {
    // alert(this.equity+this.amount+this.short+this.long+this.profitCap+this.lossCap);
    this.serviceCall.sendData(this.equity,this.amount,this.short,this.long,this.profitCap,this.lossCap)
      .subscribe(
        data => console.log("Form submitted successfully:", data),
        error => console.log('Form submission error', error));
  }


}
