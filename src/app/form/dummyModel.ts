/*
Class used as a base model for strategy parameters' inputs.
@Author Majd Asab + Rami Shehto
*/
export class DummyModel{
  constructor(
    public equity:    string,
    public amount:    number,
    public long:      number,
    public short:     number,
    public profitCap: number,
    public lossCap:   number,
  ){}

}
