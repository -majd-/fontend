/*
Component to handle active strategies monitoring and termination.
@Author Majd Asab + Rami Shehto
*/

import { Component } from "@angular/core";
import { ServiceCall} from "../serviceCall";

@Component({
  selector: "active-strategies-component",
  templateUrl: "active.strategies.component.html"
})
export class ActiveStrategiesComponent{
  constructor(private serviceCall : ServiceCall){}
  array = [];
  strategy;
  killActive: boolean = true;


  killStrategy(){
    this.serviceCall.killCommand(this.strategy).subscribe(
      data => console.log("Kill:",data),
      error => console.log("kill error:",error))
    this.strategy = undefined;
  }

  refreshList(){
    this.serviceCall.refreshActiveStrategiesList().subscribe(
      (data: any[]) => {
        if(data.length <= 0){
          console.log("Insufficient number of strategies");
        }else{
          this.array = data;
        }

      },
      error => console.log("Strategies update error",error)
    )
  }

}
