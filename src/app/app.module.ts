import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { HttpClientModule } from '@angular/common/http';
import { ServiceCall } from './serviceCall';
import { TransactionsComponent } from "./transactions/transactions.component";
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ActiveStrategiesComponent } from './active-strategies/active.strategies.component';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    TransactionsComponent,
    ActiveStrategiesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    Ng2SmartTableModule,

  ],
  providers: [ServiceCall],
  bootstrap: [AppComponent]
})
export class AppModule { }
