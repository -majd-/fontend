/*
Service component to handle http requests needed.
@Author Majd Asab + Rami Shehto
*/
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceCall {

  url = 'http://localhost:8080';
  constructor(private http: HttpClient) { }

  // form submission call
  sendData(equity: String, amount: number, short: number,long: number,profitCap: number,lossCap: number) {
    return this
      .http
      .get(`${this.url}/getStrategy?stock=${equity}&amount=${amount}&short=${short}&long=${long}&profit=${profitCap}&loss=${lossCap}`);
  }

  // database fetching call
  getAllDbInfo() {
    return this.http.get(`${this.url}/getAll`);
  }

  // strategy termination call
  killCommand(traderId:string) {
    return this.http.get(`${this.url}/getCommand?traderID=${traderId}`);
  }

  // active strategies query call
  refreshActiveStrategiesList(){
    return this.http.get(`${this.url}/listActive`);
  }




}
